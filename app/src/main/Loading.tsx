import * as BS from 'react-bootstrap';
import {Icon} from './Icon';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Rx from 'rx-dom';

interface Props<T> {
    observable: Rx.Observable<T>;
    onComplete: (T) => React.ReactElement<any>;
}

interface State<T> {
    data?: T;
    error?: any;
    subscription?: Rx.IDisposable;
}

export class Loading<T> extends React.Component<Props<T>, State<T>> {
    constructor(props: Props<T>) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const subscription = this.props.observable.subscribe(
            data => this.setState({data}),
            error => this.setState({error})
        );
        this.setState({data: null, error: null, subscription});
    }

    componentWillUnmount() {
        this.state.data = null;
        this.state.error = null;
        this.state.subscription.dispose();
    }

    render() {
        switch (true) {
        case this.state.data != null:
            return this.props.onComplete(this.state.data);
        case this.state.error != null:
            return <BS.Alert bsStyle='danger'>
                <strong>Error:</strong>{' '}
                {errorMessage(this.state.error)}
            </BS.Alert>;
        default:
            return <div><Icon name='cog' spin /></div>;
        }
    }
}

function errorMessage(error: any): string {
    try {
        if (error instanceof Error) {
            return error.message;
        } else if ('xhr' in error) {
            return '' + error.status + ' — ' + error.xhr.responseText;
        } else {
            return '' + error;
        }
    } catch (e) {
        return 'error acquiring error message';
    }
}
