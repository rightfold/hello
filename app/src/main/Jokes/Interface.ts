import * as Rx from 'rx-dom';

import {Joke} from './Joke';

export class JokeInterface {
    constructor(private endpoint: string) { }

    get allJokes(): Rx.Observable<Joke[]> {
        return Rx.DOM.ajax(this.endpoint + '/')
            .map((data: Rx.DOM.AjaxSuccessResponse) =>
                JSON.parse(data.response).map(raw => ({
                    id: raw.id,
                    object: raw.object,
                })));
    }

    saveJoke(joke: Joke): Rx.Observable<{}> {
        return Rx.DOM.ajax({
            url: this.endpoint + '/?object=' + encodeURIComponent(joke.object),
            method: 'POST',
        })
        .map(response => ({}));
    }
}
