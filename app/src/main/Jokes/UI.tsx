import * as BS from 'react-bootstrap';
import * as React from 'react';

import {Joke} from './Joke';

interface FormProps {
    onSave?: {(joke: Joke): void};
}

interface FormState {
    object: string;
}

export class JokeForm extends React.Component<FormProps, FormState> {
    constructor(props: FormProps) {
        super(props);
        this.state = {object: ''};
    }

    render() {
        return <BS.Input
            type="text"
            label="Jocular Object"
            value={this.state.object}
            onChange={ev => this.setState({object: ev.target.value})}
            buttonAfter={<BS.Button onClick={this.save.bind(this)}>
                Save
            </BS.Button>} />;
    }

    private save() {
        if (this.props.onSave != null) {
            const joke = {
                id: '' + Math.random(),
                object: this.state.object,
            };
            this.props.onSave(joke);
        }
        this.setState({object: ''});
    }
}

interface ListProps {
    jokes: Joke[];
}

interface ListState { }

export class JokeList extends React.Component<ListProps, ListState> {
    constructor(props: ListProps) {
        super(props);
    }

    render() {
        return <BS.ListGroup>
            {this.props.jokes.map(joke => <BS.ListGroupItem key={joke.id}>
                {joke.object}
            </BS.ListGroupItem>)}
        </BS.ListGroup>;
    }
}
