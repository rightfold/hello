import * as React from 'react';

interface Props {
    name: string;
    spin?: boolean;
}

interface State { }

export class Icon extends React.Component<Props, State> {
    render() {
        const classes = ['fa', 'fa-' + this.props.name];
        if (this.props.spin) {
            classes.push('fa-spin');
        }
        return <i className={classes.join(' ')}></i>;
    }
}
