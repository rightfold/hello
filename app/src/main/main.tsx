import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {JokeInterface} from './Jokes/Interface';
import {App} from './App';

addEventListener('load', () => {
    const jokeInterface = new JokeInterface('http://192.168.2.10:8080');

    ReactDOM.render(
        <App
            existingJokes={jokeInterface.allJokes}
            saveJoke={jokeInterface.saveJoke.bind(jokeInterface)} />,
        document.getElementById('app')
    );
});
