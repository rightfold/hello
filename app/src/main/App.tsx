import * as BS from 'react-bootstrap';
import * as React from 'react';
import * as Rx from 'rx-dom';

import {Joke} from './Jokes/Joke';
import {JokeForm, JokeList} from './Jokes/UI';
import {Loading} from './Loading';

interface AppProps {
    existingJokes: Rx.Observable<Joke[]>;
    saveJoke: (Joke) => Rx.Observable<{}>;
}

interface AppState { }

export class App extends React.Component<AppProps, AppState> {
    private newJokes: Rx.Subject<Joke[]>;
    private allJokes: Rx.Observable<Joke[]>;

    constructor(props: AppProps) {
        super(props);
        this.state = {};
        this.newJokes = new Rx.BehaviorSubject<Joke[]>([]);
        this.allJokes =
            Rx.Observable.merge(null, this.props.existingJokes, this.newJokes)
            .scan((js, ks) => ks.concat(js));
    }

    render() {
        return <BS.Grid fluid>
            <BS.Row>
                <BS.Col md={6}>
                    <JokeForm onSave={this.save.bind(this)} />
                </BS.Col>
                <BS.Col md={6}>
                    <Loading
                        observable={this.allJokes as any}
                        onComplete={jokes => <JokeList jokes={jokes} />}
                    />
                </BS.Col>
            </BS.Row>
        </BS.Grid>;
    }

    save(joke): void {
        this.newJokes.onNext([joke]);
        this.props.saveJoke(joke).subscribe();
    }
}
