-- Verify hello:jokes on pg

BEGIN;

SELECT id, object, created
FROM hello.jokes
WHERE false;

ROLLBACK;
