-- Deploy hello:jokes to pg
-- requires: appschema

BEGIN;

CREATE TABLE hello.jokes (
    id uuid NOT NULL,
    object text NOT NULL,
    created timestamptz NOT NULL,
    PRIMARY KEY (id),
    CHECK (object <> '')
);

COMMIT;
