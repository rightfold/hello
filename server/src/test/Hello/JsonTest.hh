<?hh // partial
namespace Hello;
use DateTimeImmutable;
use PHPUnit_Framework_TestCase;

class JsonTest extends PHPUnit_Framework_TestCase {
    public function testFromDateTime() {
        $datetime = new DateTimeImmutable('2010-01-28T15:00:00+02:00');
        $this->assertEquals(
            'Thu, 28 Jan 2010 15:00:00 +0200',
            Json::fromDateTime($datetime)
        );
    }
}
