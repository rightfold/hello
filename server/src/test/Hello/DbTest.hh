<?hh // partial
namespace Hello;
use DateTimeImmutable;
use PHPUnit_Framework_TestCase;

class DbTest extends PHPUnit_Framework_TestCase {
    public function testToDateTime() {
        $dbValue = '2016-03-05 11:24:11.155283+01';
        $this->assertEquals(new DateTimeImmutable($dbValue), Db::toDateTime($dbValue));
    }
}
