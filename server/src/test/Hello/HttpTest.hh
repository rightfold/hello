<?hh // partial
namespace Hello;
use PHPUnit_Framework_TestCase;

class HttpTest extends PHPUnit_Framework_TestCase {
    public function testJsonResponse() {
        $value = (object)['a' => [1, 2, 3]];
        $response = Http::jsonResponse($value, 404, ['X-Foo' => 'bleh']);
        $this->assertEquals($value, json_decode($response->getContent()));
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('bleh', $response->headers->get('X-Foo'));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
    }
}
