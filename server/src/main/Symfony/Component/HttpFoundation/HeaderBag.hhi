<?hh // decl
namespace Symfony\Component\HttpFoundation;

class HeaderBag implements IteratorAggregate, Countable {
    public function get(string $key, mixed $default = null, bool $first = true): mixed;
}
