<?hh // decl
namespace Symfony\Component\HttpFoundation;

class Request {
    public ParameterBag $request;
    public ParameterBag $query;
    public ParameterBag $cookies;
    public ParameterBag $attributes;
    public FileBag $files;
    public ServerBag $server;
    public HeaderBag $headers;

    public static function createFromGlobals(): Request;
}
