<?hh // decl
namespace Symfony\Component\HttpFoundation;

class Response {
    public ResponseHeaderBag $headers;

    public function __construct(mixed $content = '', int $status = 200, array<string, string> $headers = []);

    public function getStatusCode(): int;
    public function getContent(): string;

    public function prepare(Request $request): Response;
    public function send(): Response;
}
