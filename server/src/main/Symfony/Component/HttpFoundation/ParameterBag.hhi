<?hh // decl
namespace Symfony\Component\HttpFoundation;

class ParameterBag {
    public function get(string $key, mixed $default = null): mixed;
}
