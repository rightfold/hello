<?hh // strict
namespace Hello;
use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;

type JokeId = UuidInterface;

final class Joke {
    public function __construct(
        public JokeId $id,
        public string $object,
        public DateTimeImmutable $created
    ) { }
}
