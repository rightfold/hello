<?hh // strict
namespace Hello;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class JokeHandlers {
    private function __construct() { }

    public static function index(
        (function(Joke): void) $insertJoke,
        (function(): Iterator<Joke>) $allJokes,
        Request $request
    ): Response {
        $object = $request->query->get('object');
        if (is_string($object) && $object !== '') {
            $joke = new Joke(Uuid::uuid4(), $object, new DateTimeImmutable());
            $insertJoke($joke);
            return new Response("$object is funny");
        } else {
            $json = [];
            foreach ($allJokes() as $joke) {
                $json[] = (object)[
                    'id' => $joke->id,
                    'object' => $joke->object,
                    'created' => Json::fromDateTime($joke->created),
                ];
            }
            return Http::jsonResponse($json);
        }
    }
}
