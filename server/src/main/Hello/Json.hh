<?hh // strict
namespace Hello;
use DateTime;
use DateTimeInterface;

final class Json {
    private function __construct() { }

    public static function fromDateTime(DateTimeInterface $datetime): mixed {
        return $datetime->format(DateTime::RFC2822);
    }
}
