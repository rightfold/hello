<?hh // strict
namespace Hello;
use PDO;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Main {
    private function __construct() { }

    public static function main(): void {
        $db = new PDO('pgsql:host=localhost;port=5432;dbname=hello;user=postgres');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $jokeStorage = new JokeStorage($db);

        $index = function($r) use($jokeStorage) {
            return JokeHandlers::index(
                $joke ==> $jokeStorage->insertJoke($joke),
                () ==> $jokeStorage->allJokes(),
                $r
            );
        };

        $request = Request::createFromGlobals();
        $response = $index($request);
        $response->prepare($request);
        $response->send();
    }
}
