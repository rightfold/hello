<?hh // strict
namespace Hello;
use Symfony\Component\HttpFoundation\Response;

final class Http {
    private function __construct() { }

    public static function jsonResponse(mixed $value, int $status = 200, array<string, string> $headers = []): Response {
        $headers['Content-Type'] = 'application/json';
        return new Response(json_encode($value, JSON_PRETTY_PRINT), $status, $headers);
    }
}
