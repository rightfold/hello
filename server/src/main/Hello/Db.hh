<?hh // strict
namespace Hello;
use DateTimeImmutable;
use PDO;
use PDOStatement;

type DbRow = Map<string, mixed>;

final class Db {
    private function __construct() { }

    private static function prepare(PDO $db, string $sql, KeyedTraversable<string, mixed> $params = Map{}): PDOStatement {
        $statement = $db->prepare($sql);
        foreach ($params as $name => $value) {
            $statement->bindValue($name, $value);
        }
        return $statement;
    }

    public static function execute(PDO $db, string $sql, KeyedTraversable<string, mixed> $params = Map{}): void {
        $statement = self::prepare($db, $sql, $params);
        $statement->execute();
    }

    public static function query(PDO $db, string $sql, KeyedTraversable<string, mixed> $params = Map{}): Iterator<DbRow> {
        $statement = self::prepare($db, $sql, $params);
        $statement->execute();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            yield new Map($row);
        }
    }

    public static function queryOne(PDO $db, string $sql, KeyedTraversable<string, mixed> $params = Map{}): ?DbRow {
        foreach (self::query($db, $sql, $params) as $row) {
            return $row;
        }
        return null;
    }

    public static function toDateTime(mixed $value): DateTimeImmutable {
        return new DateTimeImmutable((string)$value);
    }
}
