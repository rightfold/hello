<?hh // strict
namespace Hello;
use PDO;
use Ramsey\Uuid\Uuid;

final class JokeStorage {
    public function __construct(private PDO $db) { }

    public function allJokes(): Iterator<Joke> {
        $rows = Db::query($this->db, "
            SELECT id, object, created
            FROM hello.jokes
            ORDER BY created DESC
        ");
        foreach ($rows as $row) {
            yield new Joke(
                Uuid::fromString((string)$row['id']),
                (string)$row['object'],
                Db::toDateTime($row['created'])
            );
        }
    }

    public function insertJoke(Joke $joke): void {
        Db::execute($this->db, "
            INSERT INTO hello.jokes (id, object, created)
            VALUES (:id, :object, :created)
        ", [
            ':id' => $joke->id,
            ':object' => $joke->object,
            ':created' => $joke->created->format('Y-m-d H:i:s.uP'),
        ]);
    }
}
