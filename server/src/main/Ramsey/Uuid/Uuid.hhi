<?hh // decl
namespace Ramsey\Uuid;

class Uuid implements UuidInterface {
    public static function fromString(string $name): UuidInterface;
    public static function uuid4(): UuidInterface;

    public function __toString(): string;
}
